package fr.uvsq.gl.entity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import fr.uvsq.gl.bdd.SingletonConnection;
import fr.uvsq.gl.dao.DAO;
import fr.uvsq.gl.dao.Dossier;

public class Fichier extends DAO<Fichier> implements Dossier{

	private int id_file;
	private int id_rep;
	private String nom;
	private String date;
	
	public static class Builder{

		private final String nom;
		private final String date;
		//private final int id_personne;
		private int id_rep;
		private int id_file;
		//private String type;

		public Builder(String nom, String date) {
			this.nom = nom;
			this.date = date;
		}

		public Builder id_rep(int id_rep) {
			this.id_rep = id_rep;
			return this;
		}

		public Builder id_file(int id_file) {
			this.id_file = id_file;
			return this;
		}

		public Fichier build() {
			return new Fichier(this);
		}
	}
	
	public Fichier(Builder builder) {
		this.id_file = builder.id_file;
		this.id_rep = builder.id_rep;
		this.nom = builder.nom;
		this.date = builder.date;
	}

	public String getDate() {
		return date;
	}

	public int getId_file() {
		return id_file;
	}

	public int getId_rep() {
		return id_rep;
	}

	public String getNom() {
		return nom;
	}
	
	@Override
	public void create() {
		
		try {
			Connection connection = SingletonConnection.getConnection();
			Statement stmt = connection.createStatement();
			
			String file = "CREATE TABLE Fichier " +
                  	 			"(id_file INTEGER not NULL AUTO_INCREMENT, " +
                  	 			" nom VARCHAR(50), " + 
                  	 			" id_rep INTEGER not NULL, " +
                  	 			" fichier BLOB not NULL, " +
                  	 			" FOREIGN KEY (id_rep) REFERENCES REPERTOIRE(id_rep) ON DELETE CASCADE," +
                  	 			" PRIMARY KEY (id_file))";
			
			stmt.executeUpdate(file);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(Fichier obj) {

		PreparedStatement prepare;
		try {
			prepare = connect.prepareStatement(
					"UPDATE FILE SET nom = ? WHERE id_file = ?");
			prepare.setString(1, obj.getNom());
			prepare.setInt(2, obj.getId_file());
			prepare.executeQuery();
			prepare.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Fichier obj) {
		
		PreparedStatement prepare;
		try {
			prepare = connect.prepareStatement(
					"DELETE FROM FILE WHERE id_file = ?");
			prepare.setInt(1, obj.getId_file());
			prepare.executeQuery();
			prepare.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Dossier> getAll(int id) {		
		return null;
	}

	@SuppressWarnings("unused")
	private byte[] readFile(String file) {
        ByteArrayOutputStream bos = null;
        FileInputStream fis;
        try {
            File f = new File(file);
            fis = new FileInputStream(f);
            byte[] buffer = new byte[1024];
            bos = new ByteArrayOutputStream();
            for (int len; (len = fis.read(buffer)) != -1;) {
                bos.write(buffer, 0, len);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e2) {
            System.err.println(e2.getMessage());
        }
        return bos != null ? bos.toByteArray() : null;
    }
	
	public void updatePicture(int id_file, String filename) {
        
		PreparedStatement prepare;
		
        String updateSQL = "UPDATE File "
                			+ "SET fichier = ? "
                			+ "WHERE id_file = ?";
        
        try{
        	prepare = connect.prepareStatement(updateSQL);
            
        	prepare.setBytes(1, readFile(filename));
        	prepare.setInt(2, id_file);
 
        	prepare.executeUpdate();
            System.out.println("Stored the file in the BLOB column.");
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
	
	public void readPicture(int id_file, String filename) throws IOException {
		
		String selectSQL = "SELECT fichier FROM FILE WHERE id_file = ?";
        ResultSet rs = null;
        FileOutputStream fos = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
 
        try {
            pstmt = connect.prepareStatement(selectSQL);
            pstmt.setInt(1, id_file);
            rs = pstmt.executeQuery();
 
            // write binary stream into file
            File file = new File(filename);
            fos = new FileOutputStream(file);
 
            System.out.println("Writing BLOB to file " + file.getAbsolutePath());
            while (rs.next()) {
                InputStream input = rs.getBinaryStream("fichier");
                byte[] buffer = new byte[1024];
                while (input.read(buffer) > 0) {
                    fos.write(buffer);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
 
                if (conn != null) {
                    conn.close();
                }
                if (fos != null) {
                    fos.close();
                }
 
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

	public Fichier find(Fichier obj) {
		// TODO Auto-generated method stub
		return null;
	}
}
