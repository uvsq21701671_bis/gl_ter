package fr.uvsq.gl.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import fr.uvsq.gl.bdd.SingletonConnection;
import fr.uvsq.gl.dao.DAO;
import fr.uvsq.gl.dao.Dossier;



public class SsDossier extends DAO<SsDossier> implements Dossier{

	//private ArrayList<Repertoire> listDoc;
	private int id_rep;
	private String nom;
	//private int id_personnel;
	private int parent_id;
	private String date;

	public static class Builder{

		private final String nom;
		private final String date;
		//private final int id_personne;
		private int id_rep;
		private int parent_id;
		//private String type;

		public Builder(String nom, String date) {
			this.nom = nom;
			//this.id_personne = id_personne;
			this.date = date;
		}

		public Builder id_rep(int id_rep) {
			this.id_rep = id_rep;
			return this;
		}

		public Builder parent_id(int parent_id) {
			this.parent_id = parent_id;
			return this;
		}

		public SsDossier build() {
			return new SsDossier(this);
		}
	}

	public SsDossier(Builder builder) {
		//listDoc = new ArrayList<Repertoire>();
		this.nom = builder.nom;
		this.date = builder.date;
		this.parent_id = builder.parent_id;
		//this.id_personnel = builder.id_personne;
		this.id_rep = builder.id_rep;
	}

	/*public ArrayList<Repertoire> getListFile() {
		return listDoc;
	}*/

	public int getId_rep() {
		return id_rep;
	}

	public String getNom() {
		return nom;
	}

	/*public int getId_personnel() {
		return id_personnel;
	}*/

	public int getParent_id() {
		return parent_id;
	}

	/*public void add(Repertoire repertoir) {
		this.listDoc.add(repertoir);
	}

	public void remove(Repertoire repertoir) {
		this.listDoc.remove(repertoir);
	}*/

	public String getDate() {
		return date;
	}

	@Override
	public void create() {
		//PreparedStatement prepare;
		try {
			Connection connection = SingletonConnection.getConnection();
			Statement stmt = connection.createStatement();

			String repertoire = "CREATE TABLE REPERTOIRE " +
					"(id_rep INTEGER not NULL AUTO_INCREMENT, " +
					" nom VARCHAR(50) not NULL, " + 
					" date VARCHAR(10) not NULL" +
					" parent_id INTEGER DEFAULT NULL," +
					" FOREIGN KEY (parent_id) REFERENCES REPERTOIRE(id_rep) ON DELETE CASCADE," +
					" PRIMARY KEY (id_rep))";

			stmt.executeUpdate(repertoire);
			System.out.println("Creation de la table REPERTOIRE avec succes!");

			//prepare = connection.prepareStatement("SELECT * from PERSONNE WHERE id_personne = ?");
			//prepare.setInt(1, this.getId_personnel());
			//ResultSet sr = prepare.executeQuery();

			/*if(sr.next()) {
				this.addRepertoire(this, new Personne.Builder(sr.getString("nom"), sr.getString("prenom")).id_personne(sr.getInt("id_personne")).build());
			}*/
			this.addRepertoire(this);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(SsDossier obj) {

		PreparedStatement prepare;
		try {
			prepare = connect.prepareStatement(
					"UPDATE REPERTOIRE SET nom = ? WHERE id_rep = ?");
			prepare.setString(1, obj.getNom());
			prepare.setInt(2, obj.getId_rep());
			prepare.executeQuery();
			prepare.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(SsDossier obj) {

		PreparedStatement prepare;
		try {
			prepare = connect.prepareStatement(
					"DELETE FROM REPERTOIRE WHERE id_rep = ?");
			prepare.setInt(1, obj.id_rep);
			prepare.executeQuery();
			prepare.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Dossier> getAll(int id) {

		ArrayList<Dossier> liste = new ArrayList<Dossier>();
		Connection connection = SingletonConnection.getConnection();
		try {
			PreparedStatement pr = connection.prepareStatement("WITH RECURSIVE repertoire_path (id_rep, nom, path) AS" + 
					"(SELECT id_rep, nom, nom as path" + 
					"FROM REPERTOIRE" + 
					"WHERE parent_id IS NULL" + 
					"UNION ALL" + 
					"SELECT r.id, r.nom, CONCAT(rp.path, ' > ', r.nom)" + 
					"FROM repertoire_path AS rp JOIN REPERTOIRE AS r" + 
					"ON rp.id_rep = r.parent_id )"+
					"SELECT * FROM repertoire_path ORDER BY path");
			//PreparedStatement pr2 = connection.prepareStatement("SELECT * FROM repertoire_path ORDER BY path");
			ResultSet rs = pr.executeQuery();
			if(rs.next()) {
				SsDossier sr = new SsDossier.Builder(rs.getString("nom"), rs.getString("date")).parent_id(rs.getInt("parent_id")).build();
				liste.add(sr);
			}
			pr.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return liste;
	}

	public void addRepertoire(SsDossier sr) {

		Connection connection = SingletonConnection.getConnection();
		try {
			/*PreparedStatement pr1 = connection.prepareStatement("SELECT * FROM PERSONNE WHERE id_personne = ?");
			pr1.setInt(1, p.getId_personne());
			System.out.println(p.getId_personne());
			ResultSet rs1 = pr1.executeQuery();*/

			/*PreparedStatement pr2 = connection.prepareStatement("SELECT * FROM REPERTOIRE WHERE parent_id = ?");
			pr1.setInt(1, parent_id);
			ResultSet rs2 = pr2.executeQuery();*/

			//On vérifie si la personne existe
			/*if(rs1.next()){

				PreparedStatement pr = connection.prepareStatement("INSERT INTO REPERTOIRE(nom, id_personne, parent_id) values (?, ?, ?)");
				pr.setString(1, sr.getNom());
				pr.setInt(2, rs1.getInt("id_personne"));
				pr.setInt(3, sr.getParent_id());
				pr.executeUpdate();
				System.out.println("Insertion du repertoire avec succes!");
				pr.close();

			}else {
				System.out.println("id_personne introuvable!");
			}*/
			PreparedStatement pr = connection.prepareStatement("INSERT INTO REPERTOIRE(nom, date, parent_id) values (?, ?, ?)");
			pr.setString(1, sr.getNom());
			pr.setString(2, sr.getDate());
			pr.setInt(3, sr.getParent_id());
			pr.executeUpdate();
			System.out.println("Insertion du repertoire avec succes!");
			pr.close();
			//pr1.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
