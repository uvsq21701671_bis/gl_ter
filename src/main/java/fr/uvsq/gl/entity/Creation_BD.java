package fr.uvsq.gl.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.uvsq.gl.bdd.SingletonConnection;;

public class Creation_BD {
	
public void addUser(String user, String mdp) {
		
		Connection connection = SingletonConnection.getConnection();
		try {
				PreparedStatement pr = connection.prepareStatement("insert into user(PSEUDO,MDP) values (?, ?)");
				pr.setString(1,user);
				pr.setString(2,mdp);
				pr.executeUpdate();
				pr.close();			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
public void addFichier(String nomfichier,int iddossier, String typefichier) {
		
		Connection connection = SingletonConnection.getConnection();
		try {
			/*
			PreparedStatement pr1 = connection.prepareStatement("SELECT * FROM user WHERE ID_USER = ?");
			pr1.setInt(1, id_personne);
			ResultSet rs1 = pr1.executeQuery();
			/*
			PreparedStatement pr2 = connection.prepareStatement("SELECT * FROM REPERTOIRE WHERE DOSSIER_idDOSSIER = ?");
			pr1.setInt(1, parent_id);
			ResultSet rs2 = pr2.executeQuery();
			
			
			if(rs1.next() && rs2.next()){
			*/	
				PreparedStatement pr = connection.prepareStatement("insert into fichier( nomFICHIER, idDOSSIER,typeFICHIER) values (?, ?, ?)");
				
				pr.setString(2,nomfichier);
				pr.setInt(3, iddossier);
				pr.setString(4, typefichier);
				pr.executeUpdate();
				pr.close();
				
			/*}else {
				System.out.println("id_personne introuvable!");
			}*/
			//pr1.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void addDossier(String nom,int id_personne,String date) throws SQLException  {
		
		Connection connection = SingletonConnection.getConnection();/*
		try {
			PreparedStatement pr2 = connection.prepareStatement("SELECT * FROM dossier WHERE DOSSIER_idDOSSIER = ?");
			pr2.setInt(1, parent_id);
			ResultSet rs2 = pr2.executeQuery();
			
			*/
			PreparedStatement pr2 = connection.prepareStatement("SELECT * FROM user WHERE ID_USER = ?");
			pr2.setInt(1, id_personne);
			System.out.println(id_personne);
			
			ResultSet rs2 = pr2.executeQuery();
			
			
			if(id_personne == 1 && rs2.next()){
			
				PreparedStatement pr = connection.prepareStatement("insert into dossier(nomDOSSIER, USER_idUSER,DATE) values (?, ?, ?)");
				pr.setString(1, nom);
				pr.setInt(2, id_personne);
				pr.setString(2, date);
				pr.executeUpdate();
				pr.close();
				
			}else {
				System.out.println("id_personne introuvable!");
			}
			pr2.close();
			/*
		} catch (SQLException e) {
			e.printStackTrace();
		} */
	}


}
