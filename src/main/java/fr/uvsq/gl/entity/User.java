package fr.uvsq.gl.entity;

public class User {
	private int id_user;
	private String speudo;
	private String mdp;
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getSpeudo() {
		return speudo;
	}
	public void setSpeudo(String speudo) {
		this.speudo = speudo;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public User(int id_user, String speudo, String mdp) {
		super();
		this.id_user = id_user;
		this.speudo = speudo;
		this.mdp = mdp;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
}
