package fr.uvsq.gl.dao;

import java.sql.Connection;

import fr.uvsq.gl.bdd.SingletonConnection;;

public abstract class DAO<T> {
	
	protected Connection connect = SingletonConnection.getConnection();
	
	public abstract void create();
	//public abstract T find(T obj);
	public abstract void update(T obj);
	public abstract void delete(T obj);
			
}
