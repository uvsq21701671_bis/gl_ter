package fr.uvsq.gl.dao;

import java.util.ArrayList;

public interface Dossier {
	ArrayList<Dossier> getAll(int id);
}
