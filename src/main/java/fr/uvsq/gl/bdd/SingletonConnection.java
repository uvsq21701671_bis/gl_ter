package fr.uvsq.gl.bdd;

import java.sql.Connection;
import java.sql.DriverManager;

public class SingletonConnection {

	private static Connection connection;
	
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost/fileexplorer", "root", "");
			System.out.println("Connection...");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("error");
		}
	}

	public static Connection getConnection() {
		return connection;
	}
}
